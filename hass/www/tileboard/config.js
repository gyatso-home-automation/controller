/*
 This is an example configuration file.

 COPY OR RENAME THIS FILE TO config.js.

 Make sure you use real IDs from your HA entities.
*/

var HEADER = { // https://github.com/resoai/TileBoard/wiki/Header-configuration
    styles: {
        padding: '15px 130px 15px',
        fontSize: '28px'
    },
    right: [
        {
            type: HEADER_ITEMS.WEATHER,
            styles: {
                margin: '0 0 0'
            },
            icon: '&sensor.dark_sky_icon.state',
            icons: {
                'clear-day': 'clear',
                'clear-night': 'nt-clear',
                'cloudy': 'cloudy',
                'rain': 'rain',
                'sleet': 'sleet',
                'snow': 'snow',
                'wind': 'hazy',
                'fog': 'fog',
                'partly-cloudy-day': 'partlycloudy',
                'partly-cloudy-night': 'nt-partlycloudy'
            },
            fields: {
                summary: '&sensor.dark_sky_summary.state',
                temperature: '&sensor.dark_sky_temperature.state',
                temperatureUnit: '&sensor.dark_sky_temperature.attributes.unit_of_measurement',
            }
        }
    ],
    left: [
        {
            type: HEADER_ITEMS.DATETIME,
            dateFormat: 'EEEE, LLLL dd', //https://docs.angularjs.org/api/ng/filter/date
        }
    ]
};

function temperatureTile(id, position, min = 21, max = 26) {
    return {
        position: position,
        type: TYPES.SENSOR,
        id: id,
        filter: function (value) {
            return value < min ? 'Chilly' : value < max ? 'Ok' : 'Too Hot';
        },
        unit: '',
        state: function (item, entity) {
            return entity.state + entity.attributes.unit_of_measurement;
        },
        customStyles: function (item, entity) {
            var color = entity.state < min ? '#59B3F1' : entity.state < max ? '' : '#FFD650';
            return { 'background-color': color }
        }
    };
}

function humidityTile(id, position, min = 35 , max = 55) {
    return {
        position: position,
        type: TYPES.SENSOR,
        id: id,
        filter: function (value) {
            return value < min ? 'Too Dry' : value < max ? 'Ok' : 'Humid';
        },
        unit: '',
        state: function (item, entity) {
            return entity.state + entity.attributes.unit_of_measurement;
        },
        customStyles: function (item, entity) {
            var color = entity.state < min ? '#F46F79' : entity.state < max ? '' : '#FFD650';
            return { 'background-color': color }
        }
    }
}

function lightIntensitivityTile(id, position, min = 2000 , max = 30000) {
    return {
        position: position,
        type: TYPES.SENSOR,
        id: id,
        filter: function (value) {
            return value < min ? 'Too Dark' : value < max ? 'Ok' : 'Too Bright';
        },
        unit: '',
        state: function (item, entity) {
            return entity.state + entity.attributes.unit_of_measurement;
        },
        customStyles: function (item, entity) {
            var color = entity.state < min ? '#F46F79' : entity.state < max ? '' : '#FFD650';
            return { 'background-color': color }
        }
    }
}

function soilFertilityTile(id, position, min = 350 , max = 2000) {
    return {
        position: position,
        type: TYPES.SENSOR,
        id: id,
        filter: function (value) {
            return value < min ? 'Under Fertilized' : value < max ? 'Ok' : 'Over Fertilized';
        },
        unit: '',
        state: function (item, entity) {
            return entity.state + entity.attributes.unit_of_measurement;
        },
        customStyles: function (item, entity) {
            var color = entity.state < min ? '#F46F79' : entity.state < max ? '' : '#FFD650';
            return { 'background-color': color }
        }
    }
}

function aqiTile(id, position) {
    return {
        position: position,
        type: TYPES.SENSOR,
        id: id,
        filter: function (value) {
            return value < 50 ? 'Ok' : value < 100 ? 'Average' : 'Bad';
        },
        unit: '',
        state: function (item, entity) {
            return entity.state + entity.attributes.unit_of_measurement;
        },
        customStyles: function (item, entity) {
            var color = entity.state < 50 ? '' : entity.state < 100 ? '#FFD650' : '#F46F79';
            return { 'background-color': color }
        }
    }
}

function seismicTile(id, position) {
    return {
        position: position,
        type: TYPES.SENSOR,
        id: id,
        filter: function (value) {
            return value == 0.0 ? 'None' : 'Active'
        },
        customStyles: function (item, entity) {
            return { 'background-color': entity.state == 0.0 ? '' : '#F46F79' }
        }
    }
}

function networkTile(id, position, expected, average) {
    return {
        position: position,
        type: TYPES.SENSOR,
        id: id,
        filter: function (value) {
            return value > expected ? 'Ok' : value > average ? 'Average' : 'Bad';
        },
        unit: '',
        state: function (item, entity) {
            return entity.state + entity.attributes.unit_of_measurement;
        },
        customStyles: function (item, entity) {
            var color = entity.state > expected ? '' : entity.state > average ? '#FFD650' : '#F46F79';
            return { 'background-color': color }
        }
    }
}

function batteryLevelTile(id, position, title = 'Battery Level') {
    return {
        position: position,
        type: TYPES.SENSOR_ICON,
        id: id,
        title: title,
        unit: '',
        state: function (item, entity) {
            return entity.state + entity.attributes.unit_of_measurement;
        },
        icon: function (item, entity) {
            var battery_level = entity.state || 0;
            var battery_round = parseInt(battery_level / 10) * 10;

            if (battery_round >= 100) return 'mdi-battery';
            else if (battery_round > 0) return 'mdi-battery-' + battery_round;
            else return 'mdi-battery-alert';
        },
        customStyles: function (item, entity) {
            var color = entity.state > 60 ? '' : entity.state > 30 ? '#FFD650' : '#F46F79';
            return { 'background-color': color }
        }
    };
}

function batteryTemperatureTile(id, position) {
    return {
        position: position,
        type: TYPES.SENSOR,
        id: id,
        title: 'Battery Temperature',
        filter: function (value) {
            return value < 35 ? 'Ok' : value < 40 ? 'Hot' : 'Too Hot';
        },
        unit: '',
        state: function (item, entity) {
            return entity.state + entity.attributes.unit_of_measurement;
        },
        customStyles: function (item, entity) {
            var color = entity.state < 35 ? '' : entity.state < 40 ? '#FFD650' : '#F46F79';
            return { 'background-color': color }
        }
    }
}

function motionTile(id, position) {
    return {
        position: position,
        type: TYPES.SENSOR_ICON,
        id: id,
        title: 'Motion',
        icons: {
            on: "mdi-run",
            off: "mdi-circle-outline"
        }
    }
}

function systemTile(id, position, medium, high) {
    return {
        position: position,
        type: TYPES.SENSOR,
        id: id,
        filter: function (value) {
            return value < medium ? 'Normal' : value < high ? 'High' : 'Too High';
        },
        unit: '',
        state: function (item, entity) {
            return entity.state + entity.attributes.unit_of_measurement;
        },
        customStyles: function (item, entity) {
            var color = entity.state < medium ? '' : entity.state < high ? '#FFD650' : '#F46F79';
            return { 'background-color': color }
        }
    }
}

function certTile(id, position) {
    return {
        position: position,
        type: TYPES.SENSOR,
        id: id,
        filter: function (value) {
            return value > 30 ? 'Valid' : value > 15 ? 'Refresh' : 'Expiring';
        },
        unit: '',
        state: function (item, entity) {
            return entity.state + ' ' + entity.attributes.unit_of_measurement;
        },
        customStyles: function (item, entity) {
            var color = entity.state > 30 ? '' : entity.state > 15 ? '#FFD650' : '#F46F79';
            return { 'background-color': color }
        }
    }
}

function isMobile() {
    return (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent));
};

//window.onresize = function () {
//    location.reload();
//}

var groupWidth = 3; // count of tiles horizontally
var tileMargin = 5; // px
var groupMargin = 15; // px

var tileSize = getTileSize(groupWidth, tileMargin, groupMargin);

function getTileSize(groupWidth, tileMargin, groupMargin) {
    var width = isVertical() ? window.innerWidth : window.innerHeight;
    !isVertical() ? width -= 45 : 0;
    width -= groupMargin * 2 + tileMargin * (groupWidth - 1);
    return +(width / groupWidth).toFixed(1);
}

function isVertical() {
    return window.innerWidth < window.innerHeight
}

var CONFIG = {
    customTheme: 'material', //CUSTOM_THEMES.TRANSPARENT, // CUSTOM_THEMES.TRANSPARENT, CUSTOM_THEMES.MOBILE, CUSTOM_THEMES.COMPACT, CUSTOM_THEMES.HOMEKIT, CUSTOM_THEMES.WINPHONE, CUSTOM_THEMES.WIN95
    transition: TRANSITIONS.ANIMATED_GPU, //ANIMATED or SIMPLE (better perfomance)
    entitySize: ENTITY_SIZES.SMALL, //SMALL, BIG are available
    tileSize: isMobile() ? tileSize : 150,
    tileMargin: 5,
    serverUrl: "https://HOME_DOMAIN",
    wsUrl: "wss://HOME_DOMAIN/api/websocket",
    googleApiKey: "GOOGLE_KEY", // Required if you are using Google Maps for device tracker
    debug: false, // Prints entities and state change info to the console.

    // next fields are optional 
    events: [],
    timeFormat: 24,
    menuPosition: isMobile() && !isVertical() ? MENU_POSITIONS.LEFT : MENU_POSITIONS.BOTTOM, // or LEFT
    hideScrollbar: false, // horizontal scrollbar
    groupsAlign: isMobile() ? isVertical() ? GROUP_ALIGNS.VERTICALLY : GROUP_ALIGNS.HORIZONTALLY : GROUP_ALIGNS.HORIZONTALLY, // or HORIZONTALLY

    header: isMobile() ? null : HEADER,

    pages: [
        {
            title: 'Hello page',
            icon: 'mdi-home-outline', // home icon
            groups: [
                {
                    title: 'Home',
                    width: 3,
                    height: 3,
                    items: [
                        {
                            position: [1, 0],
                            type: TYPES.DEVICE_TRACKER,
                            id: 'device_tracker.mariusz_phone',
                            bg: '@attributes.entity_picture',
                            states: {
                                home: "Home",
                                not_home: "Away",
                            }
                        },
                        {
                            position: [2, 0],
                            type: TYPES.DEVICE_TRACKER,
                            id: 'device_tracker.iphone_gabriela',
                            bg: '@attributes.entity_picture',
                            states: {
                                home: "Home",
                                not_home: "Away",
                            }
                        },
                        {
                            position: [0, 0],
                            id: 'camera.piggies_camera',
                            type: TYPES.CAMERA_THUMBNAIL,
                            bgSize: 'cover',
                            width: 1,
                            height: 1,
                            state: false,
                            fullscreen: {
                                type: TYPES.CAMERA,
                                refresh: null,
                                bgSize: 'cover',
                                filter: function (item, entity) {
                                    return '/api/camera_proxy_stream/' + entity.entity_id + '?token=' + entity.attributes.access_token;
                                }
                            },
                            refresh: 10000
                        },
                        {
                            position: [0, 1],
                            type: TYPES.SCRIPT,
                            id: 'script.show_piggies_camera',
                            state: false,
                            icon: "mdi-pig"
                        },
                        {
                            position: [0, 2],
                            type: TYPES.SCRIPT,
                            id: 'script.restart_routers',
                            state: false,
                            icon: "mdi-router-wireless"
                        },
                        {
                            position: [1, 1],
                            type: TYPES.SWITCH,
                            id: 'switch.ac',
                            states: {
                                on: "On",
                                off: "Off"
                            },
                            icons: {
                                on: "mdi-air-conditioner",
                                off: "mdi-fan-off",
                            }
                        },
                        {
                            position: [2, 1],
                            type: TYPES.SCRIPT,
                            id: 'script.ac_sleep',
                            state: false,
                            icon: "mdi-power-sleep"
                        },
                    ]
                },
                {
                    title: '',
                    width: 3,
                    height: 3,
                    items: [
                        temperatureTile('sensor.home_temperature', [0, 0]),
                        temperatureTile('sensor.fibaro_system_fgms001zw5_motion_sensor_temperature', [1, 0]),
                        temperatureTile('sensor.mitemp_bt_temperature', [2, 0]),
                        humidityTile('sensor.home_humidity', [0, 1]),
                        humidityTile('sensor.mitemp_bt_humidity', [2, 1]),
                        aqiTile('sensor.home_aqi', [1, 1]),
                        {
                            position: [0, 2],
                            type: TYPES.SENSOR_ICON,
                            title: 'Kitchen motion',
                            id: 'binary_sensor.fibaro_system_fgms001zw5_motion_sensor_sensor',
                            states: {
                                on: "On",
                                off: "Off"
                            },
                            icons: {
                                on: 'mdi-walk',
                                off: 'mdi-checkbox-blank-circle-outline'
                            },
                        },
                        networkTile('sensor.speedtest_upload', [1, 2], 6, 4),
                        networkTile('sensor.speedtest_download', [2, 2], 55, 40),
                    ]
                }
            ]
        },
        {
            title: 'Living room',
            icon: 'mdi-sofa',
            groups: [
                {
                    title: 'TV Remote',
                    width: 3,
                    height: 3,
                    items: [
                        {
                            position: [0, 0],
                            type: TYPES.SWITCH,
                            id: 'switch.tv_control',
                            states: {
                                on: "On",
                                off: "Off"
                            },
                            icons: {
                                on: "mdi-television",
                                off: "mdi-television-off",
                            }
                        },
                        {
                            position: [1, 0],
                            type: TYPES.SCRIPT,
                            id: 'script.jedynka',
                            state: false,
                            icons: {
                                on: "mdi-numeric-1-box",
                                off: "mdi-numeric-1-box",
                            }
                        },
                        {
                            position: [2, 0],
                            type: TYPES.SCRIPT,
                            id: 'script.dwojka',
                            state: false,
                            icons: {
                                on: "mdi-numeric-2-box",
                                off: "mdi-numeric-2-box",
                            }
                        },
                        {
                            position: [0, 1],
                            type: TYPES.SCRIPT,
                            id: 'script.tvn',
                            state: false,
                            icons: {
                                on: "mdi-numeric-7-box",
                                off: "mdi-numeric-7-box",
                            }
                        },
                        {
                            position: [1, 1],
                            type: TYPES.SCRIPT,
                            id: 'script.tvn24',
                            state: false,
                            icons: {
                                on: "mdi-store-24-hour",
                                off: "mdi-store-24-hour",
                            }
                        },
                        {
                            position: [2, 1],
                            type: TYPES.SCRIPT,
                            id: 'script.tvn_style',
                            state: false,
                            icons: {
                                on: "mdi-palette-swatch",
                                off: "mdi-palette-swatch",
                            }
                        },
                        {
                            position: [0, 2],
                            type: TYPES.SCRIPT,
                            id: 'script.mute_tv',
                            state: false,
                            icons: {
                                on: "mdi-volume-mute",
                                off: "mdi-volume-mute",
                            }
                        },
                        {
                            position: [1, 2],
                            type: TYPES.SCRIPT,
                            id: 'script.tv_volume_down',
                            state: false,
                            icons: {
                                on: "mdi-volume-minus",
                                off: "mdi-volume-minus",
                            }
                        },
                        {
                            position: [2, 2],
                            type: TYPES.SCRIPT,
                            id: 'script.tv_volume_up',
                            state: false,
                            icons: {
                                on: "mdi-volume-plus",
                                off: "mdi-volume-plus",
                            }
                        }
                    ]
                },
                {
                    title: 'Fan',
                    width: 2,
                    height: 3,
                    items: [
                        {
                            position: [0, 0],
                            type: TYPES.SWITCH,
                            id: 'switch.fan',
                            states: {
                                on: "On",
                                off: "Off"
                            },
                            icons: {
                                on: "mdi-fan",
                                off: "mdi-fan-off",
                            }
                        },
                        {
                            position: [1, 0],
                            type: TYPES.SCRIPT,
                            id: 'script.fan_speed',
                            state: false,
                            icons: {
                                on: "mdi-speedometer",
                                off: "mdi-speedometer",
                            }
                        },
                        {
                            position: [0, 1],
                            type: TYPES.SCRIPT,
                            id: 'script.fan_mode',
                            state: false,
                            icons: {
                                on: "mdi-chip",
                                off: "mdi-chip",
                            }
                        },
                        {
                            position: [1, 1],
                            type: TYPES.SCRIPT,
                            id: 'script.fan_moist',
                            state: false,
                            icons: {
                                on: "mdi-water-percent",
                                off: "mdi-water-percent",
                            }
                        },
                        {
                            position: [0, 2],
                            type: TYPES.SCRIPT,
                            id: 'script.fan_osc',
                            state: false,
                            icons: {
                                on: "mdi-call-split",
                                off: "mdi-call-split",
                            }
                        },
                        {
                            position: [1, 2],
                            type: TYPES.SCRIPT,
                            id: 'script.fan_timer',
                            state: false,
                            icons: {
                                on: "mdi-update",
                                off: "mdi-update",
                            }
                        }
                    ]
                },

                {
                    title: 'Lights',
                    width: 2,
                    height: 3,
                    items: [
                        {
                            position: [0, 0],
                            title: 'Ceiling',
                            subtitle: 'Lamp',
                            id: 'light.living_room_ceiling_lamp',
                            type: TYPES.LIGHT,
                            states: {
                                on: "On",
                                off: "Off"
                            },
                            icons: {
                                on: "mdi-lightbulb-on",
                                off: "mdi-lightbulb",
                            },
                            sliders: [
                                {
                                    title: 'Brightness',
                                    field: 'brightness',
                                    max: 255,
                                    min: 0,
                                    step: 5,
                                    request: {
                                        type: "call_service",
                                        domain: "light",
                                        service: "turn_on",
                                        field: "brightness"
                                    }
                                }
                            ]
                        },
                        {
                            position: [0, 1],
                            title: 'Lamps',
                            subtitle: '',
                            id: 'light.living_room_lamps',
                            type: TYPES.LIGHT,
                            states: {
                                on: "On",
                                off: "Off"
                            },
                            icons: {
                                on: "mdi-lightbulb-on",
                                off: "mdi-lightbulb",
                            },
                            sliders: [
                                {
                                    title: 'Brightness',
                                    field: 'brightness',
                                    max: 255,
                                    min: 0,
                                    step: 5,
                                    request: {
                                        type: "call_service",
                                        domain: "light",
                                        service: "turn_on",
                                        field: "brightness"
                                    }
                                },
                                {
                                    title: 'Color temp',
                                    field: 'color_temp',
                                    max: 588,
                                    min: 153,
                                    step: 15,
                                    request: {
                                        type: "call_service",
                                        domain: "light",
                                        service: "turn_on",
                                        field: "color_temp"
                                    }
                                }
                            ]
                        },
                        {
                            position: [0, 2],
                            title: 'Floor Lamp',
                            subtitle: '',
                            id: 'light.living_room',
                            type: TYPES.LIGHT,
                            states: {
                                on: "On",
                                off: "Off"
                            },
                            icons: {
                                on: "mdi-lightbulb-on",
                                off: "mdi-lightbulb",
                            },
                            sliders: [
                                {
                                    title: 'Brightness',
                                    field: 'brightness',
                                    max: 255,
                                    min: 0,
                                    step: 5,
                                    request: {
                                        type: "call_service",
                                        domain: "light",
                                        service: "turn_on",
                                        field: "brightness"
                                    }
                                },
                                {
                                    title: 'Color temp',
                                    field: 'color_temp',
                                    max: 588,
                                    min: 153,
                                    step: 15,
                                    request: {
                                        type: "call_service",
                                        domain: "light",
                                        service: "turn_on",
                                        field: "color_temp"
                                    }
                                }
                            ]
                        },
                        {
                            position: [1, 0],
                            id: 'scene.learning',
                            type: TYPES.SCENE,
                            state: false,
                            icon: 'mdi-book-open-page-variant',
                        },
                        {
                            position: [1, 1],
                            id: 'scene.evening_chill',
                            type: TYPES.SCENE,
                            state: false,
                            icon: 'mdi-sofa',
                        },
                        {
                            position: [1, 2],
                            id: 'scene.goodnight',
                            type: TYPES.SCENE,
                            state: false,
                            icon: 'mdi-weather-night',
                        }
                    ]
                },
                {
                    title: '',
                    width: 3,
                    height: 3,
                    items: [

                        {
                            position: [2, 0],
                            type: TYPES.SCRIPT,
                            id: 'script.clean_air',
                            state: false,
                            icons: {
                                on: "mdi-weather-windy",
                                off: "mdi-circle-outline",
                            }
                        },
                        {
                            position: [2, 1],
                            type: TYPES.FAN,
                            id: 'fan.xiaomi_air_purifier',
                        },
                        {
                            position: [0, 0],
                            type: TYPES.SWITCH,
                            id: 'vacuum.xiaomi_vacuum_cleaner',
                            icon: 'mdi-robot-vacuum'
                        },
                        {
                            position: [0, 1],
                            id: 'media_player.tv',
                            type: TYPES.MEDIA_PLAYER,
                            hideSource: false,
                            hideMuteButton: false,
                            state: true,
                            width: 2,
                            state: '@attributes.media_title',
                            subtitle: '@attributes.media_title',
                            bgSuffix: '@attributes.entity_picture',
                        },
                        batteryLevelTile('sensor.vacuum_battery', [1, 0]),
                        {
                            position: [0, 2],
                            id: 'media_player.salon_speaker',
                            type: TYPES.MEDIA_PLAYER,
                            hideSource: false,
                            hideMuteButton: false,
                            state: true,
                            width: 2,
                            state: '@attributes.media_title',
                            subtitle: '@attributes.media_title',
                            bgSuffix: '@attributes.entity_picture',
                        },
                        {
                            position: [2, 2],
                            type: TYPES.SCRIPT,
                            id: 'script.piggies',
                            state: false,
                            icon: "mdi-pig"
                        }
                    ]
                },
                {
                    title: 'Piggies',
                    width: 3,
                    height: 3,
                    items: [
                        {
                            position: [0, 0],
                            id: 'camera.piggies_camera',
                            type: TYPES.CAMERA_THUMBNAIL,
                            bgSize: 'cover',
                            width: 2,
                            height: 1,
                            state: false,
                            fullscreen: {
                                type: TYPES.CAMERA,
                                refresh: null, // can be number in milliseconds
                                bgSize: 'cover',
                                filter: function (item, entity) {
                                    return '/api/camera_proxy_stream/' + entity.entity_id + '?token=' + entity.attributes.access_token;
                                }
                            },
                            refresh: 10000
                        },
                        {
                            position: [2, 0],
                            type: TYPES.SCRIPT,
                            id: 'script.restart_piggies_camera',
                            state: false,
                            icon: "mdi-backup-restore"
                        },
                        batteryLevelTile('sensor.piggies_camera_battery', [0, 1]),
                        batteryTemperatureTile('sensor.piggies_camera_battery_temperature', [1, 1]),
                        {
                            position: [2, 1],
                            type: TYPES.SENSOR,
                            id: 'sensor.piggies_camera_video_connections',
                            title: 'Video Connections'
                        },
                        {
                            position: [0, 2],
                            type: TYPES.SWITCH,
                            id: 'switch.piggies_camera_front_facing_camera',
                            states: {
                                on: "Off",
                                off: "On"
                            },
                            icons: {
                                on: "mdi-camera-off",
                                off: "mdi-camera",
                            },
                            title: 'Camera'
                        },
                        {
                            position: [1, 2],
                            type: TYPES.SWITCH,
                            id: 'switch.piggies_camera_torch',
                            states: {
                                on: "On",
                                off: "Off"
                            },
                            icons: {
                                on: "mdi-flashlight",
                                off: "mdi-flashlight-off",
                            },
                            title: 'Torch'
                        },
                        motionTile('binary_sensor.piggies_camera_motion_active', [2, 2])
                    ]
                },
                {
                    title: 'Orchid',
                    width: 2,
                    height: 3,
                    items: [
                        soilFertilityTile('sensor.mi_flora_conductivity', [0, 0]),
                        lightIntensitivityTile('sensor.mi_flora_light_intensity', [0, 1]),
                        humidityTile('sensor.mi_flora_moisture', [1, 0], 15, 65),
                        temperatureTile('sensor.mi_flora_temperature', [1, 1], 15, 32),
                        batteryLevelTile('sensor.mi_flora_battery', [0, 2])
                    ]
                }
            ]
        },
        {
            title: 'Kitchen',
            icon: 'mdi-fridge', // home icon
            groups: [
                {
                    title: '',
                    width: 3,
                    height: 3,
                    items: [
                        {
                            position: [0, 0],
                            type: TYPES.SENSOR_ICON,
                            id: 'binary_sensor.neo_coolcam_door_window_detector_sensor',
                            title: 'Fridge Doors',
                            states: {
                                on: "Open",
                                off: "Closed"
                            },
                            icons: {
                                on: "mdi-door-open",
                                off: "mdi-door-closed",
                            },
                        },
                        batteryLevelTile('sensor.fridge_doors_sensor_battery', [1, 0]),
                        {
                            position: [2, 0],
                            type: TYPES.AUTOMATION,
                            title: 'Alert',
                            subtitle: 'When Fridge Doors are Open',
                            id: 'automation.alert_when_fridge_doors_are_open',
                            icon: 'mdi-home-alert'
                        },
                        {
                            position: [0, 1],
                            type: TYPES.SENSOR,
                            id: 'sensor.fibaro_system_fgwpef_wall_plug_gen5_energy'
                        },
                        {
                            position: [1, 1],
                            type: TYPES.SENSOR,
                            id: 'sensor.fibaro_system_fgwpef_wall_plug_gen5_power'
                        },
                        {
                            position: [2, 1],
                            type: TYPES.SWITCH,
                            id: 'switch.fibaro_system_fgwpef_wall_plug_gen5_switch',
                            states: {
                                on: "On",
                                off: "Off"
                            },
                            icons: {
                                on: "mdi-power-socket-eu",
                                off: "mdi-square",
                            }
                        },
                        batteryLevelTile('sensor.motion_sensor', [0, 2], 'Bathroom sensor battery')
                    ]
                },
                {
                    title: '',
                    width: 3,
                    height: 3,
                    items: [
                        motionTile('binary_sensor.fibaro_system_fgms001zw5_motion_sensor_sensor', [0, 0]),
                        batteryLevelTile('sensor.motion_sensor_battery', [0, 1]),
                        {
                            position: [1, 0],
                            type: TYPES.SWITCH,
                            id: 'script.clean_kitchen',
                            states: {
                                on: "On",
                                off: "Off"
                            },
                            icons: {
                                on: "mdi-fridge",
                                off: 'mdi-robot-vacuum',
                            }
                        },
                        {
                            position: [0, 2],
                            type: TYPES.SENSOR,
                            title: 'Kitchen luminace',
                            id: 'sensor.fibaro_system_fgms001zw5_motion_sensor_luminance',
                        },
                        {
                            position: [1, 2],
                            title: 'Bedroom Lamp',
                            subtitle: '',
                            id: 'light.bedroom',
                            type: TYPES.LIGHT,
                            states: {
                                on: "On",
                                off: "Off"
                            },
                            icons: {
                                on: "mdi-lightbulb-on",
                                off: "mdi-lightbulb",
                            },
                            sliders: [
                                {
                                    title: 'Brightness',
                                    field: 'brightness',
                                    max: 255,
                                    min: 0,
                                    step: 5,
                                    request: {
                                        type: "call_service",
                                        domain: "light",
                                        service: "turn_on",
                                        field: "brightness"
                                    }
                                },
                                {
                                    title: 'Color temp',
                                    field: 'color_temp',
                                    max: 588,
                                    min: 153,
                                    step: 15,
                                    request: {
                                        type: "call_service",
                                        domain: "light",
                                        service: "turn_on",
                                        field: "color_temp"
                                    }
                                }
                            ]
                        },
                        temperatureTile('sensor.mitemp_bt_temperature', [2, 0]),
                        humidityTile('sensor.mitemp_bt_humidity', [2, 1]),
                        batteryLevelTile('sensor.mitemp_bt_battery', [2, 2])
                    ]
                }
            ]
        },
        {
            title: 'System',
            icon: 'mdi-desktop-classic', // raspberrypi
            groups: [
                {
                    title: 'Raspberry',
                    width: 3,
                    height: 3,
                    items: [
                        systemTile('sensor.disk_use_percent', [0, 0], 55, 70),
                        systemTile('sensor.memory_use_percent', [1, 0], 60, 80),
                        systemTile('sensor.processor_use', [2, 0], 20, 50),
                        certTile('sensor.home_cert_validitiy', [0, 1]),
                        {
                            position: [1, 1],
                            type: TYPES.SENSOR,
                            id: 'sensor.uptime',
                            state: false
                        },
                        {
                            position: [0, 2],
                            type: TYPES.SENSOR,
                            id: 'sensor.speedtest_ping',
                        },
                        networkTile('sensor.speedtest_upload', [1, 2], 6, 4),
                        networkTile('sensor.speedtest_download', [2, 2], 55, 40)
                    ]
                },
                {
                    title: '',
                    width: 2,
                    height: 3,
                    items: [
                        {
                            position: [1, 0],
                            type: TYPES.AUTOMATION,
                            title: 'Notification',
                            subtitle: 'New version HASS',
                            id: 'automation.hass_update_available_notifications',
                            icon: 'mdi-autorenew'
                        },
                        {
                            position: [1, 1],
                            type: TYPES.SENSOR,
                            id: 'sensor.hass_version',
                        },
                        certTile('sensor.blog_cert_validitiy', [0, 2]),
                        {
                            position: [1, 2],
                            type: TYPES.SENSOR_ICON,
                            id: 'sensor.gitlab_ci_status',
                            customStyles: function (item, entity) {
                                var result = {};
                                if (entity.state !== 'success') {
                                    result['background-color'] = '#F46F79';
                                }
                                return result;
                            },
                            icon: function (item, entity) {
                                return entity.attributes.icon.replace('mdi:', 'mdi-')
                            }
                        }
                    ]
                },
                {
                    title: 'Z-wave',
                    width: 2,
                    height: 3,
                    items: [
                        {
                            position: [0, 0],
                            type: TYPES.SENSOR,
                            id: 'zwave.aeotec_zw090_zstick_gen5_eu',
                        },
                        {
                            position: [1, 0],
                            type: TYPES.SENSOR,
                            id: 'zwave.fibaro_system_fgms001zw5_motion_sensor',
                        },
                        {
                            position: [0, 1],
                            type: TYPES.SENSOR,
                            id: 'zwave.fibaro_system_fgwpef_wall_plug_gen5',
                        },
                        {
                            position: [0, 2],
                            type: TYPES.SENSOR,
                            id: 'zwave.neo_coolcam_door_window_detector',
                        }
                    ]
                }
            ]
        },
        {
            title: 'Weather',
            icon: 'mdi-weather-partlycloudy',
            groups: [
                {
                    title: 'Weather',
                    width: 3,
                    height: 2,
                    items: [
                        {
                            position: [0, 0],
                            height: 2,
                            //classes: ['-compact'], // enable this if you want a little square tile (1x1)
                            type: TYPES.WEATHER,
                            id: {},
                            state: '&sensor.dark_sky_summary.state', // label with weather summary (e.g. Sunny)
                            icon: '&sensor.dark_sky_icon.state',
                            //iconImage: '&sensor.dark_sky_icon.state', // use this one if you want to replace icon with image
                            icons: {
                                'clear-day': 'clear',
                                'clear-night': 'nt-clear',
                                'cloudy': 'cloudy',
                                'rain': 'rain',
                                'sleet': 'sleet',
                                'snow': 'snow',
                                'wind': 'hazy',
                                'fog': 'fog',
                                'partly-cloudy-day': 'partlycloudy',
                                'partly-cloudy-night': 'nt-partlycloudy'
                            },
                            fields: { // most of that fields are optional
                                summary: '&sensor.dark_sky_summary.state',
                                temperature: '&sensor.dark_sky_temperature.state',
                                temperatureUnit: '&sensor.dark_sky_temperature.attributes.unit_of_measurement',
                                windSpeed: '&sensor.dark_sky_wind_speed.state',
                                windSpeedUnit: '&sensor.dark_sky_wind_speed.attributes.unit_of_measurement',
                                humidity: '&sensor.dark_sky_humidity.state',
                                humidityUnit: '&sensor.dark_sky_humidity.attributes.unit_of_measurement',

                                list: [
                                    // custom line
                                    'Feels like '
                                    + '&sensor.dark_sky_apparent_temperature.state'
                                    + '&sensor.dark_sky_apparent_temperature.attributes.unit_of_measurement',

                                    // another custom line
                                    'Pressure '
                                    + '&sensor.dark_sky_pressure.state'
                                    + '&sensor.dark_sky_pressure.attributes.unit_of_measurement',

                                    // yet another custom line
                                    '&sensor.dark_sky_precip_probability.state'
                                    + '&sensor.dark_sky_precip_probability.attributes.unit_of_measurement'
                                    + ' chance of rain'
                                ]
                            }
                        },
                        {
                            position: [1, 0],
                            type: TYPES.WEATHER_LIST,
                            width: 2,
                            height: 1,
                            title: 'Forecast',
                            id: {},
                            icons: {
                                'clear-day': 'clear',
                                'clear-night': 'nt-clear',
                                'cloudy': 'cloudy',
                                'rain': 'rain',
                                'sleet': 'sleet',
                                'snow': 'snow',
                                'wind': 'hazy',
                                'fog': 'fog',
                                'partly-cloudy-day': 'partlycloudy',
                                'partly-cloudy-night': 'nt-partlycloudy'
                            },
                            hideHeader: false,
                            secondaryTitle: 'Wind',
                            list: [1, 2, 3].map(function (id) {
                                var d = new Date(Date.now() + id * 24 * 60 * 60 * 1000);
                                var date = d.toString().split(' ').slice(1, 3).join(' ');

                                var forecast = "&sensor.dark_sky_overnight_low_temperature_" + id + "d.state - ";
                                forecast += "&sensor.dark_sky_daytime_high_temperature_" + id + "d.state";
                                forecast += "&sensor.dark_sky_daytime_high_temperature_" + id + "d.attributes.unit_of_measurement";

                                var wind = "&sensor.dark_sky_wind_speed_" + id + "d.state";
                                wind += " &sensor.dark_sky_wind_speed_" + id + "d.attributes.unit_of_measurement";

                                return {
                                    date: date,
                                    icon: "&sensor.dark_sky_icon_" + id + "d.state",
                                    //iconImage: null, replace icon with image
                                    primary: forecast,
                                    secondary: wind
                                }
                            })
                        }
                    ]
                },
                {
                    title: 'Air Quality',
                    width: 2,
                    height: 2,
                    items: [
                        {
                            position: [0, 0],
                            type: TYPES.SENSOR,
                            id: 'sensor.us_air_pollution_level',
                        },
                        aqiTile('sensor.us_air_quality_index', [1, 0]),
                        {
                            position: [0, 1],
                            type: TYPES.SENSOR,
                            id: 'sensor.us_main_pollutant',
                        },
                    ]
                }
            ]
        }
    ],
}
