#!/bin/bash
set -e

function update_proxy {
  echo "Pulling proxy version $1..."
  wget -O /tmp/archive.jar https://gitlab.com/api/v4/projects/gyatso-home-automation%2Foauth2proxy/jobs/artifacts/$1/download?job=release
  unzip -o /tmp/archive.jar -d /tmp/
  echo "Deploying $1..."
  sudo systemctl stop oauth2proxy
  mv /tmp/app.jar /opt/oauth2proxy/app.jar
  chmod +x /opt/oauth2proxy/app.jar
  sudo systemctl start oauth2proxy
  echo $1 > /home/pi/.proxy-version
  echo "Proxy $1 deployed successfully."
}

function restart_proxy {
  echo "Version of proxy matches. Restarting proxy..."
  sudo systemctl restart oauth2proxy
  echo "Proxy restarted successfully."
}

function restart_hass {
  echo "Version of hass matches. Restarting hass..."
  docker restart home-assistant
  echo "Proxy restarted successfully."
}

function update_hass {
  echo "Pulling hass version $1..."
  docker pull homeassistant/raspberrypi3-homeassistant:$1
  docker stop home-assistant || echo 'Nothing to stop'
  docker rm home-assistant || echo 'Nothing to remove'
  echo "Deploying $1..."
  docker run -d --name="home-assistant" -v /opt/hass:/config -v /etc/localtime:/etc/localtime:ro --net=host --device /dev/ttyACM0:/dev/ttyACM0 --privileged --restart always homeassistant/raspberrypi3-homeassistant:$1
  echo $1 > /home/pi/.hass-version
  echo "Hass $1 deployed successfully."
}

[ -z $1 ] && echo "Usage: ./update.sh <proxy version> <hass version>" && exit 1
[ -z $2 ] && echo "Usage: ./update.sh <proxy version> <hass version>" && exit 1

# [ -f /home/pi/.proxy-version ] && [ `cat /home/pi/.proxy-version` == $1 ] && restart_proxy
# [ ! -f /home/pi/.proxy-version ] && update_proxy $1
# [ `cat /home/pi/.proxy-version` != $1 ] && update_proxy $1

# bash -c 'while [[ "$(curl -s -o /dev/null -w ''%{http_code}'' localhost:8080)" != "401" ]]; do sleep 5; done'

[ "$(docker ps -a | grep home-assistant)" ] && update_hass $2
[ -f /home/pi/.hass-version ] && [ `cat /home/pi/.hass-version` == $2 ] && restart_hass
[ ! -f /home/pi/.hass-version ] && update_hass $2
[ `cat /home/pi/.hass-version` != $2 ] && update_hass $2

bash -c 'while [[ "$(curl -s -o /dev/null -w ''%{http_code}'' localhost:8123)" != "200" ]]; do sleep 5; done'

exit 0