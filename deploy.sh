#!/usr/bin/env bash

#fswatch hass/www/tileboard/styles/custom.less | xargs -n1 deploy.sh

echo "Compiling"
lessc hass/www/tileboard/styles/custom.less hass/www/tileboard/styles/custom.css

cp hass/www/tileboard/config.js hass/www/tileboard/config2.js
sed -i.bak "s/PASSWORD_KEY/$HASS_TOKEN/g" hass/www/tileboard/config2.js

echo "Deploying"
scp hass/www/tileboard/styles/custom.css pi@$HOME_DOMAIN:/opt/hass/www/tileboard/styles/custom.css
scp hass/www/tileboard/config2.js pi@192.168.31.100:/opt/hass/www/tileboard/config.js

rm hass/www/tileboard/config2.js
rm hass/www/tileboard/config2.js.bak