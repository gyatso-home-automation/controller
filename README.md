# Home Assistant Configuration

This repository contains my home assistant configuration with some extra features

* [List of devices](https://docs.google.com/presentation/d/1LO5pMpsoGax2h-RfSmzlCbY4UZWheGAUfcQcNzlOCpQ/edit?usp=sharing)
* [Series of posts describing my setup](https://blog.luciow.pl/automation/2018/01/07/new-years-resolution/)
* [Home Assistant](https://www.home-assistant.io/)
* [TileBoard](https://github.com/resoai/TileBoard/), alternative UI 